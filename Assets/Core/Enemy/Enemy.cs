﻿public class Enemy : Player {

    public IState CurState { get; set; }

    public Easy EasyState;
    public Medium MediumState;
    public Hard HardState;

    public Model model;

    private void Start ()
    {
        model = GetComponent<Model>();
        EasyState = new Easy();
        MediumState = new Medium();
        HardState = new Hard();
        CurState = EasyState;
    }

    private void Update () {
        if (model.Step == Model.step.Player2)
        {
            CurState.StateUpdate(this);
        }
    }
}
