﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Veiw : MonoBehaviour {

    [SerializeField]
    Animator Anim = new Animator();
    [SerializeField]
    Controller controller;

    [SerializeField]
    Text FinalText;
    [SerializeField]
    Dropdown dropdown;

    private void Start()
    {
        GameObject.Find("StartButton").GetComponent<Button>().onClick.AddListener(() => OnMatchStart());
        GameObject.Find("ExitButton").GetComponent<Button>().onClick.AddListener(() => OnExit());
        dropdown = GameObject.Find("Complexity").GetComponent<Dropdown>();
        dropdown.onValueChanged.AddListener((a) => OnComplexityChange(a));
    }

    private void OnLevelWasLoaded(int level)
    {
        if (level == 1)
        {
            controller.InitializeComponent();
            Anim = FindObjectOfType<Animator>();
            FinalText = GameObject.Find("FinalText").GetComponent<Text>();
            GameObject.Find("MenuButton").GetComponent<Button>().onClick.AddListener(() => OnMenuExit());
            GameObject.Find("RestartButton").GetComponent<Button>().onClick.AddListener(() => OnStartMatch());
            Cell[] cells = FindObjectsOfType<Cell>();
            foreach (Cell cell in cells)
            {
                cell.GetComponent<Button>().onClick.AddListener(() => OnCellClick(cell.index));
            }
        }

        if (level == 0) {
            GameObject.Find("StartButton").GetComponent<Button>().onClick.AddListener(() => OnMatchStart());
            GameObject.Find("ExitButton").GetComponent<Button>().onClick.AddListener(() => OnExit());
            dropdown = GameObject.Find("Complexity").GetComponent<Dropdown>();
            dropdown.onValueChanged.AddListener((a) => OnComplexityChange(a));
        }
    }

    public void OnCellClick(int index)
    {
        controller.CellClick(index);
    }

    public void OnComplexityChange(int complex) {
        controller.ComplexityChange(complex);
    }

    public void OnMatchStart()
    {
        controller.LoadMatch();
    }

    public void Win()
    {
        Anim.SetBool("Final", true);
    }

    public void OnStartMatch()
    {
        controller.StartMatch();
    }

    public void SetFinalPanelActive(bool active) {
        Anim.SetBool("Final", active);
    }

    //public void OnStatsChange()
    //{
    //
    //}

    public void OnMenuExit()
    {
        controller.GoToMenu();
    }

    public void OnExit()
    {
        controller.Exit();
    }

    public void OnFinal(string message) {
        FinalText.text = message;
        Anim.SetBool("Final", true);
    }
}
