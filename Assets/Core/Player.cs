﻿using System;
using UnityEngine.UI;
using UnityEngine;

public class Player : MonoBehaviour {

    [Serializable]
    public struct UI
    {
        public Text Victory;
        public Text Defeat;
        public Text Draw;
    }

    public struct Stats
    {
        public Int32 Victory;
        public Int32 Defeat;
        public Int32 Draw;
    }

    public UI PlayerUI;
    public Stats PlayerStats;
    
}
