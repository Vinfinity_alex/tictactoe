﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

    [SerializeField]
    Model model;
    [SerializeField]
    Veiw veiw;

    public void InitializeComponent() {
        model.InitializeComponent();
    }

    public void CellClick(int index)
    {
        model.CellClick(index);
    }

    public void GoToMenu()
    {
        model.GoToMenu();
    }

    public void LoadMatch()
    {
        model.LoadMatch();
    }

    public void Exit()
    {
        model.Exit();
    }

    public void Final(string message) {
        veiw.OnFinal(message);
    }

    public void StartMatch()
    {
        model.StartMatch();
    }

    public void SetFinalPanelActive(bool active)
    {
        veiw.SetFinalPanelActive(active);
    }

    public void ComplexityChange(int value) {
        model.Complexity = value;
    }
}
