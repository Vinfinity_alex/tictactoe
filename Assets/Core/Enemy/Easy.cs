﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Easy : IState
{
    public void StateUpdate(Enemy enemy) {
        List<Cell> cells = enemy.model.GetFreeCells();
        if (cells.Count != 0)
        {
            int rand = Random.Range(0, cells.Count);
            enemy.model.CellClick(cells[rand].index);
        }
    }
}
