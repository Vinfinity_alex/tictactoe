﻿using UnityEngine;

public interface IState {
    void StateUpdate(Enemy enemy);
}
