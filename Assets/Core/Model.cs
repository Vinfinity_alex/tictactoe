﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Model : MonoBehaviour {

    public static Model instance = null;

    [SerializeField]
    Controller controller;

    [SerializeField]
    List<Cell> Cells;

    [Serializable]
    public struct Vector3 {

        public Vector3(int _x, int _y, int _z) {
            x = _x;
            y = _y;
            z = _z;
        }

        public int x;
        public int y;
        public int z;
    }
    [SerializeField]
    public Vector3[] WinIndexs;

    Player Player;
    Enemy Enemy;

    public enum match { None, Draw, Win, Lose}
    match Match;

    public enum  step { Player1, Player2 }
    public step Step;
    
    public bool isCross;
    
    public int Complexity;

    int Victory = 0;
    int Defeat = 0;
    int Draw = 0;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(this);
    }
 

    //UI
    public void InitializeComponent()
    {
        Cells = FindObjectsOfType<Cell>().ToList<Cell>();
        Cells.Sort((x, y) => x.index.CompareTo(y.index));
        Enemy = GetComponent<Enemy>();
        Enemy.enabled = true;
        ComplexityChange(Complexity);
        Player = GetComponent<Player>();

        Player.PlayerUI.Victory = GameObject.Find("Player1WinTxt").GetComponent<Text>();
        Player.PlayerUI.Defeat = GameObject.Find("Player1DefeatTxt").GetComponent<Text>();
        Player.PlayerUI.Draw = GameObject.Find("Player1DrawTxt").GetComponent<Text>();
        Enemy.PlayerUI.Victory = GameObject.Find("Player2WinTxt").GetComponent<Text>();
        Enemy.PlayerUI.Defeat = GameObject.Find("Player2DefeatTxt").GetComponent<Text>();
        Enemy.PlayerUI.Draw = GameObject.Find("Player2DrawTxt").GetComponent<Text>();
    }
    
    public void CellClick(int index) {

        if (isCross)
        {
            Cells[index].State = Cell.state.Cross;
            Cells[index].txt.text = "X";
        }
        else
        {
            Cells[index].State = Cell.state.Zero;
            Cells[index].txt.text = "0";
        }
        
        isCross = !isCross;
        Cells[index].btn.interactable = false;

        if (ChekingForFinal())
        {
            if(Step == step.Player1)Final(match.Win);
            if (Step == step.Player2) Final(match.Lose);
            
            return;
        }

        if (ChekingForDraw())
        {
            Final(match.Draw);
            return;
        }
        NextPlayer();
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadMatch()
    {        
        SceneManager.LoadScene(1);
        StartMatch();
    }

    public void StartMatch() {

        foreach (Cell c in Cells)
        {
            c.btn.interactable = true;
            c.txt.text = "";
            c.State = Cell.state.Null;
        }
        
        switch (Match) {
            case match.Draw:
                NextPlayer();
                break;
            case match.Win:
                Step = step.Player1;
                break;
            case match.Lose:
                Step = step.Player2;
                break;
        }

        Match = match.None;
        ComplexityChange(Complexity);
        isCross = true;
        controller.SetFinalPanelActive(false);
        Enemy.enabled = true;
    }

    public void Exit()
    {
        Application.Quit();
    }

    void UpdateStats()
    {
        Player.PlayerUI.Victory.text = Victory.ToString();
        Player.PlayerUI.Defeat.text = Defeat.ToString();
        Player.PlayerUI.Draw.text = Draw.ToString();
        Enemy.PlayerUI.Victory.text = Defeat.ToString();
        Enemy.PlayerUI.Defeat.text = Victory.ToString();
        Enemy.PlayerUI.Draw.text = Draw.ToString();
    }
    

    //Logic
    bool ChekingForDraw() {
        if (GetFreeCells().Count == 0 && !ChekingForFinal()) return true;

        return false;
    }

    bool ChekingForFinal(){

        List<int> cross = new List<int>();
        List<int> zero = new List<int>();

        for (int i = 0; i < Cells.Count; i++) {
            if (Cells[i].State == Cell.state.Cross)
            {
                if(!cross.Contains(i)) cross.Add(i);
            }
        
            if (Cells[i].State == Cell.state.Zero)
            {
                if (!zero.Contains(i)) zero.Add(i);
            }
        }

        zero.Sort();
        cross.Sort();

        foreach (Vector3 vect in WinIndexs)
        {
            for (int i = 0; i < cross.Count; i++)
                for (int j = 0; j < cross.Count; j++)
                    for (int u = 0; u < cross.Count; u++)
                    {
                        if (cross[i] == vect.x && cross[j] == vect.y && cross[u] == vect.z)
                        {
                            return true;
                        }
                    }

            for (int i = 0; i < zero.Count; i++)
                for (int j = 0; j < zero.Count; j++)
                    for (int u = 0; u < zero.Count; u++)
                    {
                        if (zero[i] == vect.x && zero[j] == vect.y && zero[u] == vect.z)
                        {
                            return true;
                        }
                    }
        }

        return false;
    }

    public void Final(match MatchStatus) {
        Enemy.enabled = false;
        foreach (Cell c in Cells) {
            c.btn.interactable = false;
        }
        switch (MatchStatus) {
            case match.Draw:
                controller.Final("Draw!");
                Draw++;
                Match = match.Draw;
                controller.SetFinalPanelActive(true);
                UpdateStats();
                break;
            case match.Lose:
                controller.Final("You lose!");
                Defeat++;
                Match = match.Lose;
                controller.SetFinalPanelActive(true);
                UpdateStats();
                break;
            case match.Win:
                controller.Final("You won!");
                Victory++;
                Match = match.Win;
                controller.SetFinalPanelActive(true);
                UpdateStats();
                break;
        }


        //if (ChekingForDraw())
        //{
        //    controller.Final("Draw!");
        //    Draw++;
        //    //PlayerStep = true;
        //    Match = match.Draw;
        //    controller.SetFinalPanelActive(true);
        //    UpdateStats();
        //    return;
        //}
        //else
        //{
        //    if (ChekingForWin() && !PlayerStep)
        //    {
        //        controller.Final("You won!");
        //        Victory++;
        //        PlayerStep = true;
        //        Match = match.Win;
        //        controller.SetFinalPanelActive(true);
        //        UpdateStats();
        //        return;
        //    }
        //    else
        //    {
        //        controller.Final("You lose!");
        //        Defeat++;
        //        PlayerStep = true;
        //        Match = match.Lose;
        //        controller.SetFinalPanelActive(true);
        //        UpdateStats();
        //        return;
        //    }
        //
        //}

    }

    public void NextPlayer() {
        switch (Step) {
            case step.Player1:
                Step = step.Player2;
                break;
            case step.Player2:
                Step = step.Player1;
                break;
        }
    }

    //Enemy
    public void ComplexityChange(int complex)
    {
        switch (complex)
        {
            case 0:
                Enemy.CurState = Enemy.EasyState;
                break;
            case 1:
                Enemy.CurState = Enemy.MediumState;
                break;
            case 2:
                Enemy.CurState = Enemy.HardState;
                break;
        }
    }

    public List<Cell> GetFreeCells() {
        List<Cell> cells = new List<Cell>();
        foreach (Cell cell in Cells)
        {
            if(cell.State == Cell.state.Null) cells.Add(cell);
        }
        return cells;
    }

    public List<Cell> GetCells()
    {
        return Cells;
    }
}
