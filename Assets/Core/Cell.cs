﻿using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class Cell : MonoBehaviour
{
    public enum state { Null, Zero, Cross }
    public Button btn;
    public Text txt;
    public state State;
    public int index;

    private void Start()
    {
        btn = GetComponent<Button>();
        txt = GetComponentInChildren<Text>();
    }
}
