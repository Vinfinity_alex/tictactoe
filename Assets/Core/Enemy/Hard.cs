﻿using System.Collections.Generic;
using UnityEngine;
using Vector3 = Model.Vector3;

public class Hard : IState
{
    Vector3 CurTactics;

    public void StateUpdate(Enemy enemy)
    {
        List<Cell> cells = enemy.model.GetFreeCells();
        if (cells.Count == 9)
        {
            int r = Random.Range(0, enemy.model.WinIndexs.Length - 1);
            CurTactics = enemy.model.WinIndexs[r];
            enemy.model.CellClick(cells[CurTactics.x].index);
        }
        else if (cells.Count == 8)
        {
            enemy.model.CellClick(OptimalPath(enemy, cells).x);
        }
        else if (cells.Count == 7)
        {
            enemy.model.CellClick(OptimalPath(enemy, cells).x);
        }
        else if (cells.Count == 6)
        {            
            enemy.model.CellClick(OptimalPath(enemy, cells).x);
        }
        else if (cells.Count == 5)
        {
            enemy.model.CellClick(OptimalPath(enemy, cells).x);
        }
        else if (cells.Count == 4)
        {
            enemy.model.CellClick(OptimalPath(enemy, cells).x);
        }
        else if (cells.Count == 3)
        {
            enemy.model.CellClick(OptimalPath(enemy, cells).x);
        }
        else if (cells.Count == 2)
        {
            enemy.model.CellClick(OptimalPath(enemy, cells).x);
        }
        else if (cells.Count == 1)
        {
            enemy.model.CellClick(OptimalPath(enemy, cells).x);
        }
    }

    Vector3 OptimalPath(Enemy enemy, List<Cell> freeCells)
    {

        foreach (Vector3 vect in enemy.model.WinIndexs)
        {
            for (int i = 0; i < freeCells.Count; i++)
                for (int j = 0; j < freeCells.Count; j++)
                    for (int u = 0; u < freeCells.Count; u++)
                    {
                        if (freeCells[i].index == vect.x && freeCells[j].index == vect.y && freeCells[u].index == vect.z)
                        {
                            int r = Random.Range(0, 5);
                            switch (r)
                            {
                                case 0:
                                    return new Vector3(vect.y, vect.x, vect.z);
                                case 1:
                                    return new Vector3(vect.x, vect.z, vect.y);
                                case 2:
                                    return new Vector3(vect.z, vect.x, vect.y);
                                case 3:
                                    return new Vector3(vect.z, vect.y, vect.x);
                                case 4:
                                    return new Vector3(vect.x, vect.y, vect.z);
                                case 5:
                                    return new Vector3(vect.y, vect.z, vect.y);
                            }
                        }
                    }
        }

        return new Model.Vector3(0, 1, 2);
    }

    int AssumptionIndex(Enemy enemy, List<Cell> cells)
    {
        if (enemy.model.isCross)
        {
            List<int> cross = new List<int>();
            for (int i = 0; i < cells.Count; i++)
            {
                if (cells[i].State == Cell.state.Cross)
                {
                    if (!cross.Contains(i)) cross.Add(i);
                }
            }
    
            cross.Sort();

            foreach (Vector3 vect in enemy.model.WinIndexs)
            {
                for (int i = 0; i < cross.Count; i++)
                    for (int j = 0; j < cross.Count; j++)
                        for (int u = 0; u < cross.Count; u++)
                        {
                            if (cross[i] == vect.x && cross[j] == vect.y)
                            {
                                return vect.y;
                            }

                            if (cross[i] == vect.x && cross[u] == vect.z)
                            {
                                return vect.z;
                            }

                            if (cross[j] == vect.y && cross[u] == vect.z)
                            {
                                return vect.x;
                            }
                        }
            }
        }
        else
        {
            List<int> zero = new List<int>();
            for (int i = 0; i < cells.Count; i++)
            {
                if (cells[i].State == Cell.state.Cross)
                {
                    if (!zero.Contains(i)) zero.Add(i);
                }
            }

            zero.Sort();

            foreach (Vector3 vect in enemy.model.WinIndexs)
            {
                for (int i = 0; i < zero.Count; i++)
                    for (int j = 0; j < zero.Count; j++)
                        for (int u = 0; u < zero.Count; u++)
                        {
                            if (zero[i] == vect.x && zero[j] == vect.y)
                            {
                                return vect.y;
                            }

                            if (zero[i] == vect.x && zero[u] == vect.z)
                            {
                                return vect.z;
                            }

                            if (zero[j] == vect.y && zero[u] == vect.z)
                            {
                                return vect.x;
                            }
                        }
            }
        }

        return 0;
    }

}

